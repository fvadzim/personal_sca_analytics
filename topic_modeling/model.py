import os, sys
import numpy as np
import copy
import pickle
import pandas as pd
from scipy.spatial.distance import cosine
cur_file_path = os.path.abspath(__file__)
sys.path.append(cur_file_path)
sys.path.append(os.path.dirname(cur_file_path))
sys.path.append(os.path.dirname(os.path.dirname(cur_file_path)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(cur_file_path))))
from filter.filter import get_filter


class Model:
    '''
        Represents PLSA(https://en.wikipedia.org/wiki/Probabilistic_latent_semantic_analysis) model, but using some regulirizers (
        https://bigartm.readthedocs.io/en/v0.8.1/python_interface/regularizers.html).
    '''
    def __init__(
            self, phi_matrix: pd.DataFrame, theta_matrix: pd.DataFrame,
            process_number: int, iterations_via_document: int, iterations_via_collection: int,
            regularizators: dict, name: str, background_topic_number: int = 0, topic_names: list = None):
        self.__phi_matrix = phi_matrix
        if topic_names:
            self.__phi_matrix.columns = pd.core.indexes.base.Index(
                                                topic_names)
        self.__topic_matrix = self.__phi_matrix.transpose()
        self.__topic_names = tuple(self.__topic_matrix.index)
        self.__theta_matrix = theta_matrix

        self.__topic_number = phi_matrix.shape[1]  # int
        self.__process_number = process_number  # int
        self.__iterations_via_document = iterations_via_document  # int
        self.__iterations_via_collection = iterations_via_collection  # int
        self.__regulirizers = regularizators
        self.__name = name
        self.__background_topic_number = background_topic_number
        self.__default_token_distribution = pd.Series(
            np.zeros(self.__topic_number), index=self.__topic_names)

    def get_phi(self):
        return self.__phi_matrix

    def get_theta(self):
        return self.__theta_matrix

    def get_topic_number(self):
        return copy.copy(self.__topic_number)

    def get_topic_matrix(self):
        return self.__topic_matrix

    def get_topic_names(self):
        return self.__topic_names

    def get_process_number(self):
        return copy.copy(self.__process_number)

    def get_iterations_via_document(self):
        return copy.copy(self.__iterations_via_document)

    def get_iterations_via_collection(self):
        return copy.copy(self.__iterations_via_collection)

    def get_regulirizers(self):
        return copy.copy(self.__regulirizers)

    def get_name(self):
        return copy.copy(self.__name)

    def get_default_token_distribution(self):
        return self.__default_token_distribution

    def get_token_distribution(self, token, modality_num=1):
        try:
            token_distribution = self.__phi_matrix.loc[token]
            if type(token_distribution) == pd.core.frame.DataFrame:
                return token_distribution.iloc[modality_num]
            return token_distribution
        except KeyError:
            return self.__default_token_distribution

    def save(self, file_to_be_saved_in):
        with open(file_to_be_saved_in, "wb") as file_to_be_pickled_in:
            pickle.dump(self, file_to_be_pickled_in)


class TopicDestribution:
    def __init__(self, model, lang):
        self.__filter = get_filter(lang)
        self.__model = model


    def get_distribution(self, text):
        """
        :param text: str, unfiltered text
        :return: topic destribution of input text(normalized sum of token distribution in text)
        """
        tokens = self.__filter.get_tokens(text)
        if not len(tokens):
            return self.__model.get_default_token_distribution()

        result_topic_vector = sum(
            self.__model.get_token_distribution(token) for token in tokens)

        return result_topic_vector / sum(result_topic_vector)

    def get_distribution_fast(self, text, delimiter= ' '):
        """
        :param text: str, space-divided (for ex. "dog sitting in front of me")
        :return: topic destribution of input text(normalized sum of token distribution in text)
        """
        tokens = text.split(delimiter)
        if not len(tokens):
            return self.__model.get_default_token_distribution()

        result_topic_vector = sum(
            self.__model.get_token_distribution(token) for token in tokens)

        return result_topic_vector / sum(result_topic_vector)



    @staticmethod
    def get_similarity(distribution_first, distribution_second):
        """
        returns cosine similarity of two vectors
        :rtype: float
        """
        return 1 - cosine(distribution_first, distribution_second)

    def get_most_similar_distribution(self, text, verbose=True):
        '''
        :param text: str
        :param verbose: bool
        :return: bool, True if there is introspection between expected set of topics and set found by model.
        '''
        text_distribution = self.get_distribution(text)
        if verbose:
            print()
            pprint.pprint(text)
            print()
            pprint.pprint("FILTERED TEXT : ")
            print(self.__filter.get_all_tokens(text))
            pprint.pprint("DESTRIBUTION : ")
            pprint.pprint(sorted(list(zip(self.__model.get_topic_names(), text_distribution)), key= lambda x:x[1], reverse=True))
        if tuple(text_distribution.values) == tuple(
                self.__model.get_default_token_distribution().values):
            return {}
        return text_distribution.nlargest(5).index

if __name__ == "__main__":
    human_names_dict = {
    "background_topic_0": "общая_тема_быт",
    "background_topic_1":
    "общая_тема_Беларусь_страна",
    "background_topic_2":
    "общая_тема_Беларусь_туризм",
    "background_topic_3":
    "общая_тема_экономика",
    "background_topic_4":
    "общая_тема_питание_рестораны",
    "background_topic_5":
    "общая_тема_увлечение",
    "background_topic_6":
    "общая_тема_культура",
    "objective_topic_0": "производство",
    "objective_topic_1": "литература",
    "objective_topic_2": "передвижения_туризм",
    "objective_topic_3": "рынок_бизнес",
    "objective_topic_4": "развлечения_мода",
    "objective_topic_5": "беларусь_государство",
    "objective_topic_6": "семья_здоровье",
    "objective_topic_7": "искусство",
    "objective_topic_8": "город",
    "objective_topic_9": "музыка",
    "objective_topic_10": "кинематограф",
    "objective_topic_11": "текст_на_белоруссоком",
    "objective_topic_12": "кофейни_рестораны",
    "objective_topic_13": "школа_молодёжь",
    "objective_topic_14": "сми_новости_РБ",
    "objective_topic_15": "пивная_культура",
    "objective_topic_16": "фотография_искусство",
    "objective_topic_17": "бизнес",
    "objective_topic_18": "белорусская_лексика",
    "objective_topic_19": "кулинария"
    }
    topic_names_example = [
        "общая_тема_передвижение",
        "общая_тема_Беларусь",
        "общая_тема_Минск",
        "музыка",
        "фестивали",
        "тенденции в моде",
        "белорусские предлоги",
        "тенденции в it",
        "рестораны",
        "кино и искусство в РБ",
        "секс",
        "одежда",
        "туризм",
        "семья",
        "быт",
        "беларусь_государство",
        "концерт_спектакль_событие",
        "беларусь_мир",
        "архитектура",
        "фотография",
        "бизнес",
        "мировоззрение",
        "политика_литература",
        "какая-то_ерунда",
        "алкоголь",
        "интернет_порталы",
        "беларусь"
    ]
    # model = artm.ARTM(num_topics=27)
    # model.load(filename="devided_model_made_properly")
    # my_model = Model(
    #     phi_matrix=model.get_phi(),
    #     theta_matrix="nothing",
    #     process_number=4,
    #     iterations_via_collection=10,
    #     iterations_via_document=10,
    #     regularizators={},
    #     name="first_of_this_class",
    #     background_topic_number=3,
    #     topic_names=topic_names_example
    # )
    my_model = None
    with open("kyky_model_26_June_12_30", 'rb') as kyky_file:
        my_model = pickle.load(kyky_file)
    print((my_model.get_token_distribution("беларусь")))
    topic_destribution = TopicDestribution(my_model)

    # print(topic_destribution.get_most_similar_distribution(
    #     '''На настоящий момент Бьорк разработала свой собственный эклектичный
    #     музыкальный стиль, который включает в себя аспекты электроники, классики,
    #     авангардной музыки, альтернативной танцевальной музыки, рока и джаза. Бьорк
    #     написала музыку к фильму «Танцующая в темноте» Ларса фон Триера, в котором
    #     сыграла главную роль и получила приз как лучшая актриса на Каннском кинофестивале.
    #     Также она приняла участие в качестве композитора и актрисы в фильме «Drawing Restraint 9»
    #     своего тогдашнего мужа Мэттью Барни, саундтрек к которому был выпущен отдельным одноимённым альбомом.
    #     В 2010 году Бьорк получила премию «Polar Music Prize» от Шведской королевской академии музыки за её «глубоко
    #     личные музыку и слова, её точные аранжировки и её уникальный голос». Всего у артистки более ста наград и двухсот
    #     номинаций. Её альбом «Biophilia» (2011) был первым альбомом в формате мобильных приложений и позднее его
    #     включили в состав постоянной коллекции Нью-Йоркского музея Современного искусства (MoMA), почти через год там
    #     прошла выставка по мотивам творчества Бьорк, которая охватила весь период её сольной карьеры. Издание «Time»
    #     включило Бьорк в раздел «иконы» как одну из 100 самых влиятельных людей в мире, назвав её «верховной жрицей
    #     искусств», небольшой текст к нему написала Марина Абрамович, утверждая, что «Бьорк учит нас храбрости быть
    #     самими собой». Музыкальные критики всегда отстаивали работу Бьорк, восхваляя её инновационный подход к пению и
    #     композиторству, её передовое использование электронных битов, прогрессивные музыкальные видео, и прежде всего —
    #     её уникальный голос, описывая её как «самого важного и дальновидного музыканта своего поколения».[2][3][4]'''))
    #
    # print(topic_destribution.get_most_similar_distribution(
    #
    # '''— Если проблем по взаимным поездкам граждан Российской Федерации и Республики Беларусь внутри Союзного государства нет, то мы поставили задачу урегулировать вопрос по пересечению внутренней границы гражданами третьих стран. Сейчас разрабатывается это соглашение, — рассказывает замначальника Главного управления по вопросам миграции Министерства внутренних дел России Дмитрий Демиденко. — Почему так долго идет работа? Потому что за последние десятилетия наши миграционные законодательства, наверное, стали разниться на 20%, хоть на 80% они и схожи. Поэтому сталкиваемся с определенными нюансами, но работа идет.
#
# — По белорусской визе, например, в Беларуси можно находиться 90 дней в течение одного года, — добавляет начальник департамента по гражданству и миграции МВД Беларуси Алексей Бегун. — В России по визе можно находиться 90 дней за полгода. Вот такие различия. Дело в том, что Российская Федерация связана соглашениями с ЕС, у нас пока нет соглашений.
#
# Сроков, когда у Беларуси и России может появиться соглашение о взаимном признании виз, не называют.
#
# Ранее глава российского МИД Сергей Лавров говорил, что соглашение о взаимном признании виз облегчит пересечение российско-белорусской границы гражданами третьих стран. Речь идет о том, чтобы иностранец, въезжающий в одну из двух стран по визе, выданной этой самой страной, мог с этой визой переместиться в другую страну.
#
# «Соответственно, чтобы те, кому визу не выдают по соображениям безопасности или иным причинам, так же не были приняты в соседнем государстве. Мы также предлагаем использовать опыт, зафиксированный в Шенгенском соглашении, который уже доказал свою высокую эффективность. Это будет обсуждаться», — добавил глава МИД России.
#
# Напомним, с 2014 года российские дипломаты заговорили о возможности появления единой визы Союзного государства — мини-«шенгена» для двух стран. В марте 2015 года об этом сказал Владимир Путин. В сентябре того же года — премьер Дмитрий Медведев. Тогда в его словах прозвучали первые намеки на то, что это поможет бороться с нелегальной миграцией.
#
# Сообщалось, что документ будет касаться только тех государств, с которыми у Беларуси и России одинаковый визовый режим. Остальные иностранцы из 80 стран мира, посещающие Беларусь по 5-дневному безвизовому режиму, поехать в Россию по-прежнему не смогут.
# Читать полностью:  https://news.tut.by/society/592763.html'''))
#
#     print(topic_destribution.get_most_similar_distribution(
#     '''Как Еврокомиссия хочет изменить охрану авторских прав
# В Директиве есть специальный раздел 2 «Определенное использование охраняемого контента онлайн-сервисами», который состоит из одной единственной статьи 13 «Использование охраняемого контента поставщиками услуг информационного общества, предоставляющими доступ к большому количеству произведений и иных объектов, загружаемых их пользователями».
#
# Thumb       123213
# THUMB 123213
# на эту тему:
# Секс, партизаны и каннибализм. Что советская цензура вырезала из книг Алексиевич
# По сути эта статья обязывает те же соцсети, поисковики и другие аналогичные сервисы (их владельцев/администрацию) предпринимать вместе с правообладателями меры по обеспечению действия соглашений, заключенных с последними в отношении использования их произведений или иных объектов. К таким мерам относятся, в частности, технологии распознавания контента. Поставщики услуг должны информировать правообладателей о мерах, которые ими предпринимаются, а также отчитываться о выявленных фактах использования охраняемого контента.
#
# '''
#     ))
#
    from classifier.classifier import Classifier
    topics_dict = {
        "1": (
            # "секс",
            # "семья",
            # "быт"
            "background_topic_0",
            "background_topic_2",
            "objective_topic_6"

        ),

        "2": (
            # "быт", "беларусь",
            #   "общая_тема_передвижение",
            #   "общая_тема_Беларусь",
            #   "общая_тема_Минск"

              "objective_topic_2",
              "background_topic_0",
              "background_topic_1",
              "background_topic_2",
              "background_topic_3",
              "background_topic_3",
              "background_topic_6",
              ),

        "3": (
            # "общая_тема_Минск", "фестивали",
            #   "рестораны", "архитектура",
            #   "кино и искусство в РБ"
            "background_topic_1",
              "background_topic_2",
              "background_topic_4",
              "background_topic_5",
              "objective_topic_8",
              "objective_topic_12",
              "objective_topic_7",
              "objective_topic_10",
              "objective_topic_1",
              "background_topic_6"
              ),

        "4": (
            # "общая_тема_передвижение",
            #   "общая_тема_Беларусь",
            #   "общая_тема_Минск",
            #   "беларусь_государство",
            #   "беларусь",
            #   "быт"
            "background_topic_1",
            "background_topic_2",
             "objective_topic_3",
              "objective_topic_2",
              "objective_topic_8",
              "objective_topic_14",
              "objective_topic_5",
              "background_topic_0",
              ),

        "5": (
            # "музыка",
            #   "фестивали",
            #   "фотография",
            #   "кино и искусство в РБ",
              "objective_topic_9",
              "objective_topic_16",
              "objective_topic_7",
              "objective_topic_10",
              "background_topic_1?"
              ),
        "6": (
            # "бизнес"
            "background_topic_3",
            "objective_topic_3",
            "objective_topic_0",
            "objective_topic_17"
        ),

        "7": (
            # "бизнес", "тенденции в it", "тенденции в моде",
            #   "кино и искусство в РБ",
              "background_topic_3",
              "objective_topic_3",
              "objective_topic_0",
              "objective_topic_17",
              "objective_topic_16",
              "objective_topic_7",
              "objective_topic_10",
               "background_topic_5",
               "background_topic_1"
              ),
        "8": (
            # "концерт_спектакль_событие",
            #   "мировоззрение", "музыка",
            #   "фестивали"
            "objective_topic_9",
            "objective_topic_10",
            "background_topic_5",
            "background_topic_2",
            "objective_topic_19",
            "objective_topic_15"
              ),
        "9": (
            # "архитектура", "фотография",
            #   "кино и искусство в РБ",
            #   "мировоззрение"
                "objective_topic_8",
                "objective_topic_5",
                "objective_topic_10",
                "objective_topic_7",
                "background_topic_1",
                "objective_topic_16"
              ),
        "10": (
            # "секс", "семья", "быт"
                "background_topic_0",
                "background_topic_2"
               ),
        "11": (
            # "тенденции в it", "мировоззрение"
                "objective_topic_16",
                "objective_topic_14",
                "background_topic_0"
               ),
        "12": (
            # "одежда"
                    "background_topic_5",
                    "objective_topic_16"

        ),
        "13": (
            # "кино и искусство в РБ",
         "objective_topic_7",
         "background_topic_1",
         "objective_topic_16"
        ),
        "14": (
            # "политика_литература",
            # "кино и искусство в РБ",
            # "мировоззрение"
                      "objective_topic_5",
                      "objective_topic_14",
                      "objective_topic_7",
                      "objective_topic_1",
                      "objective_topic_10",
                      "background_topic_1",
                      "background_topic_2"
        ),
        "15": (
            # "тенденции в it",
            # "интернет_порталы",
            # "беларусь"
            "objective_topic_3",
            "background_topic_1",
            "background_topic_3"
            "objective_topic_14",
            "background_topic_6",
            "objective_topic_8",

        ),
        "16": (
            # "фотография"
            "objective_topic_7",
            "objective_topic_16",
        ),
        "17": (
            # "общая_тема_передвижение",
            # "фестивали",
            "objective_topic_2",
            "background_topic_4",
            "background_topic_2",
            "objective_topic_9"
        ),
        "18": (
            # "беларусь",
            # "общая_тема_Беларусь",
            # "общая_тема_Минск",
            # "быт",
            # "политика"
            # "беларусь_мир"
            "background_topic_1",
            "background_topic_3",
            "objective_topic_14",
            "background_topic_6",
            "objective_topic_8",
            "objective_topic_14",
            "objective_topic_13",
            "background_topic_0",
            "background_topic_2"
        ),
        "19": (
            # "общая_тема_Минск",
            # "архитектура",
            "background_topic_5",
            "background_topic_3",
            "background_topic_2",
            "objective_topic_8"
        ),
        "20": (
            # "концерт_спектакль_событие",
            # "фестивали",
            "background_topic_1",
            "objective_topic_10",
            "objective_topic_9",
            "objective_topic_5"
        ),
        "21": (),
        "22": (
            # "быт"
                "background_topic_0",
                "background_topic_1"
        ),
        "23": (
            # "общая_тема_Минск",
            # "туризм", "бизнес",
            # "беларусь_государство",
            "background_topic_2",
            "background_topic_1",
            "background_topic_4",
            "background_topic_6",
            "objective_topic_3",
            "objective_topic_5",
            "objective_topic_12",
            "objective_topic_0",
            "objective_topic_2",
            "objective_topic_8",
            "objective_topic_17",
            "objective_topic_14"
        ),

        "24": (
            # "общая_тема_передвижение",
            # "общая_тема_Беларусь",
            # "общая_тема_Минск",
            # "туризм",
            # "беларусь",
            # "фестивали",
            # "мировоззрение",
            "objective_topic_4",
            "background_topic_4",
            "objective_topic_15"
            "background_topic_2",
            "objective_topic_2",
            "background_topic_1",
            "background_topic_6",
            "objective_topic_3",
            "objective_topic_5",
            "objective_topic_12",
            "objective_topic_0",
            "objective_topic_2",
            "objective_topic_8",
            "objective_topic_17",
            "objective_topic_14",
            "objective_topic_9",
        ),

        "25": (
            # "общая_тема_передвижение",
            # "общая_тема_Беларусь",
            # "общая_тема_Минск",
            # "туризм",
            # "беларусь",
            # "фестивали",
            # "мировоззрение",
            "objective_topic_4",
            "background_topic_4",
            "objective_topic_15"
         "objective_topic_2",
         "objective_topic_16",
         "objective_topic_5",
         "objective_topic_14",
         "objective_topic_19",
         "background_topic_4",
         "objective_topic_9",
        ),

        "26": (),
        "27": (
            # "тенденции в it",
            "objective_topic_16"
            "background_topic_1"
        ),

        "28": (
            # "общая_тема_передвижение",
            # "общая_тема_Беларусь",
            # "общая_тема_Минск",
             "objective_topic_2",
             "background_topic_1",
            "background_topic_6",
             "objective_topic_3",
            "objective_topic_5",
            "objective_topic_8",
            "objective_topic_13",
            "objective_topic_14",
             "background_topic_3",
            "background_topic_5"
        ),

        "29": (),

        "30": (
            # "интернет_порталы",
            # "тенденции в it",
            # "общая_тема_Беларусь",
            # "общая_тема_Минск",
            "objective_topic_16",
            "objective_topic_3",
            "objective_topic_14",
            "background_topic_1",
            "background_topic_3",
            "background_topic_6",
            "objective_topic_8"
        ),
        "31": (
            # "рестораны",
            # "кино и искусство в РБ",
            # "туризм",
            # "концерт_спектакль_событие",
            # "фестивали",
            "objective_topic_7",
            "objective_topic_1",
            "objective_topic_15",
            "objective_topic_19",
            "objective_topic_9",
            "objective_topic_10",
            "objective_topic_12",
            "objective_topic_4",
            "background_topic_4",
             "objective_topic_15"

           ),
        "32": (),

        "33": (
            # "общая_тема_Беларусь",
            # "беларусь_государство",
            # "беларусь"
            "background_topic_1",
            "background_topic_3",
            "background_topic_6",
            "objective_topic_3",
            "objective_topic_5",
             "objective_topic_8",
            "background_topic_2",
                  "objective_topic_12"
        ),

        "34": (
            # "общая_тема_передвижение",
            # "общая_тема_Беларусь",
            # "общая_тема_Минск",
            # "туризм",
            # "беларусь"
             "background_topic_2",
             "background_topic_1",
             "background_topic_6",
             "objective_topic_3",
            "objective_topic_5",
            "objective_topic_12",
             "background_topic_4"
            
        ),
    }
    my_classifier = Classifier(topics_dict, topic_destribution)

    from censor.keyword_extraction.keyword import Rake, TextRank, TfIdf
    from censor.convert.cur_csv_to_vw import *
    tf_idf = TfIdf(pickle.load(open(os.environ["HOME"] + '/BSUIR/server_part/censor/data/tf_idf.pickle', 'rb')))
    tf_idf.document_number = 3980
    rake = Rake('ru')
    text_rank = TextRank()
    import  pprint

    import csv
    with open(os.environ["HOME"] + '/BSUIR/server_part/censor/data/contents.csv', 'r') as csv_file:
         # csv_reader = csv.DictReader(csv_file, '\t')
         headers = get_headers(csv_file, '\t')
         csv_reader = get_dict_reader(csv_file, headers, '\t')
         for text_dict in csv_reader:
             # print(text_dict['content'])
             # pprint.pprint("TEXT : \n")
             pprint.pprint( [human_names_dict[topic_name] for topic_name in
                             topic_destribution.get_most_similar_distribution(text_dict['content'], verbose=True)])

             pprint.pprint("KeyPhrases : \n")
             pprint.pprint(rake.get_key_words(text_dict['content']))
             pprint.pprint("TopWords : \n")
             pprint.pprint(tf_idf.get_key_words(text_dict['content']))
             pprint.pprint("KeyPhrases smaller: \n")
             pprint.pprint(text_rank.get_key_words(text_dict['content']))



    # print(my_classifier.classify("{}/BSUIR/server_part/censor/data/contents.csv".format(os.environ["HOME"])))


