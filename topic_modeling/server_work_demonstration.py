import os, sys
import numpy as np
import copy
import pickle
import pandas as pd
from scipy.spatial.distance import cosine
cur_file_path = os.path.abspath(__file__)
sys.path.append(cur_file_path)
sys.path.append(os.path.dirname(cur_file_path))
sys.path.append(os.path.dirname(os.path.dirname(cur_file_path)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(cur_file_path))))
from servers import *

# dsitribtion server

import Pyro4
distribution_server = Pyro4.Proxy("PYRO:master@127.0.0.1:9090")

text_about_politics = '''Political science is a social science which deals with systems of governance, and the analysis of political activities, political thoughts, and political behavior.[1] It deals extensively with the theory and practice of politics which is commonly thought of as determining of the distribution of power and resources. Political scientists "see themselves engaged in revealing the relationships underlying political events and conditions, and from these revelations they attempt to construct general principles about the way the world of politics works."[2]

Political science -- occasionally called politicology -- comprises numerous subfields, including comparative politics, political economy, international relations, political theory, public administration, public policy, and political methodology. Furthermore, political science is related to, and draws upon, the fields of economics, law, sociology, history, philosophy, geography, psychology/psychiatry, and anthropology.

Comparative politics is the science of comparison and teaching of different types of constitutions, political actors, legislature and associated fields, all of them from an intrastate perspective. International relations deals with the interaction between nation-states as well as intergovernmental and transnational organizations. Political theory is more concerned with contributions of various classical and contemporary thinkers and philosophers.

Political science is methodologically diverse and appropriates many methods originating in social research. Approaches include positivism, interpretivism, rational choice theory, behaviouralism, structuralism, post-structuralism, realism, institutionalism, and pluralism. Political science, as one of the social sciences, uses methods and techniques that relate to the kinds of inquiries sought: primary sources such as historical documents and official records, secondary sources such as scholarly 
journal articles, survey research, statistical analysis, case studies, experimental research, and model building.'''

print(distribution_server.get_topic_distribution(text_about_politics))
