import numpy as np
import pickle

class Statistic:
    def __init__(self):
        raise NotImplementedError

    def add_sample(self):
        raise NotImplementedError

    def get_value(self):
        return self.value__

    def get_samples_count(self):
        return len(self.samples__)


class MostPopularTopics(Statistic):
    '''
        pleas use numpy arrays
    '''
    def __init__(self, dimension, top_k):
        self.top_k_ = top_k
        self.dimension_ = dimension
        self.cnt_ = 0
        self.value_ = np.zeros(self.dimension_)

    def add_sample(self, distribution):
        assert distribution.shape[0] == self.dimension_
        indexes = np.argpartition(a, -self.top_k_)[-self.top_k_:]
        self.value_[indexes] += distribution[indexes]

    def get_value(self):
        return self.value_

    def get_samples_count(self):
        return self.cnt_

    def save(self, file_to_be_saved_in):
        with open(file_to_be_saved_in, "wb") as file_to_be_pickled_in:
            pickle.dump(self, file_to_be_pickled_in)


class MostPopularTopics(Statistic):
    '''
        please use numpy arrays
    '''
    def __init__(self, dimension, top_k):
        self.top_k_ = top_k
        self.dimension_ = dimension
        self.cnt_ = 0
        self.value_ = np.zeros(self.dimension_)

    def add_sample(self, distribution):
        assert distribution.shape[0] == self.dimension_
        indexes = np.argpartition(a, -self.top_k_)[-self.top_k_:]
        self.value_[indexes] += distribution[indexes]
        self.cnt_ += 1

    def get_value(self):
        return self.value_

    def get_samples_count(self):
        return self.cnt_

class DifferentAuthorsCnt(Statistic):
    '''
        pleas use numpy arrays
    '''
    def __init__(self, dimension, top_k):
        self.top_k_ = top_k
        self.dimension_ = dimension
        self.cnt_ = 0
        self.value_ = np.zeros(self.dimension_)

    def add_sample(self, distribution, author_cnt):
        assert distribution.shape[0] == self.dimension_
        indexes = np.argpartition(a, -self.top_k_)[-self.top_k_:]
        self.value_[indexes] += author_cnt
        self.cnt_ += 1

    def get_value(self):
        return self.value_

    def get_samples_count(self):
        return self.cnt_
