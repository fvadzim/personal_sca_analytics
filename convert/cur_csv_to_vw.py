# -*- coding:utf-8 -*-
import os, sys
cur_file_path = os.path.abspath(__file__)
print("cur file path: ", cur_file_path)
sys.path.append(cur_file_path)
sys.path.append(os.path.dirname(cur_file_path))
sys.path.append(os.path.dirname(os.path.dirname(cur_file_path)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(cur_file_path))))

print(sys.path)
import multiprocessing
import json
import os
import argparse
import csv
import functools
from analytics.convert.text_process import *
from analytics.trie import trie

csv.field_size_limit(500 * 1024 * 1024)
locker = multiprocessing.Lock()

'''
    Module main goal is to get folowwing format from dict representation.
    *Following format* is https://github.com/JohnLangford/vowpal_wabbit/wiki/Input-format.
'''


class VwReader:
    def __init__(self, file_, splitter, id_at_beginning=True):
        self.__file = file_
        self.__splitter = splitter
        self.__id_at_beginning = id_at_beginning

    def __iter__(self):
        return self.next()

    @staticmethod
    def get_word_cnt_from_bow(bow):
        """
        :param bow:str bag_of_words representation .ex: token_0: 2 token_2 token_3: 4
        :return python dict
        """
        word_cnt = dict()
        for token in bow.strip().split(' '):
            token_name, *token_freq = token.split(':')
            if token_freq:
                word_cnt[token_name] = int(token_freq[0])
            else:
                word_cnt[token_name] = 1
        return word_cnt

    def next(self):
        for line in self.__file:
            dict_ = dict()
            for modality in line.split(self.__splitter)[bool(self.__id_at_beginning):]:
                modality_name, *tokens = modality.strip().split(' ')
                dict_[modality_name] = dict()
                for token in tokens:
                    token_name, *token_freq = token.split(':')
                    if token_freq:
                        dict_[modality_name][token_name] = int(token_freq[0])
                    else:
                        dict_[modality_name][token_name] = 1
            yield dict_


def get_texts_from_json_files(path_to_folder):
    """
    :param path_to_folder: unix path to json file
    :return list for readed json files
    """
    json_list = (
        json.loads(abs_file_name) for abs_file_name in [
        os.path.join(path_to_folder, file_name) for
        file_name in os.listdir(path_to_folder)
    ]
    )
    pool = multiprocessing.Pool(20)
    pool.map(write_json_row_to_vw_file, json_list)
    pool.close()
    pool.join()


def write_json_row_to_vw_file(row, vw_path):
    with locker:
        with open(vw_path, "a") as vw_file:
            vw_file.write(post_to_corpus_line(row))


def write_dict_row_to_vw_file_atomic(row, vw_path, **kwargs):
    with locker:
        with open(vw_path, "a") as vw_file:
            vw_file.write(post_to_corpus_line(row, **kwargs))


def write_in_parallel_to_vw(vw_file, dict_rows):
    '''
    use parallel(but not very fast) writing to vowpal wabbit file
    :param vw_file:
    :param dict_rows:
    :return:
    '''
    write_dict_row_to_vw_file_default = functools.partial(
        write_dict_row_to_vw_file_atomic, vw_path=vw_file)
    p = multiprocessing.Pool(10)
    return p.map(write_dict_row_to_vw_file_default, dict_rows)


def get_vw_rows_from_csv_rows(dict_rows):
    p = multiprocessing.Pool(10)
    return p.map(post_to_corpus_line, dict_rows)


def get_csv_rows_materialized(reader_csv):
    return [row for row in reader_csv]


def delete_stop_words_from_wv_file(file_path):
    '''
    It is not good idea to use this funciton. Try to provide your text with proper filtering before creating vw file.
    Hovewer it is cheaper to fix than to create new one.
    :param file_path:
    :return:
    '''
    lines = []
    #stop_words_trie = trie.load_trie("stopwords.marisa")
    with open(file_path) as wv_file:
        for line in wv_file:
            refactored_line = [word for word in line.split() if not len(word.split(":")[0]) == 1]# in stop_words_trie]
            refactored_line.append("\n")
            lines.append(" ".join(refactored_line))
    with open(file_path, "w") as wv_file:
        wv_file.writelines(lines)


def get_dict_reader(input_file, fieldnames, delimiter='\t'):
    """
    creates csv readed which reads only specified fields.
    :param fieldnames: names of fields to read.
    :rtype: object
    """
    return csv.DictReader(
        input_file,
        fieldnames=fieldnames,
        delimiter=delimiter
    )


def get_headers(input_file, delimiter='\t'):
    '''

    :param input_file: path to input fie
    :param delimiter: delimet of csv file
    :return: list of headers of input file
    '''
    return input_file.readline().strip().split(delimiter)


def convert_csv_column_to_json_id_set(file_path, column_name, delimiter='\t'):
    columns_values_set = dict()
    id_index = 1
    with open(file_path, "r") as csv_file:
        headers = get_headers(csv_file, delimiter)
        for row in get_dict_reader(csv_file, headers):
            if not columns_values_set.get(row[column_name], 0):
                columns_values_set[row[column_name]] = (
                    column_name + "_{0}".format(id_index,))
                id_index += 1
    return columns_values_set


def main():
    parser = argparse.ArgumentParser(description='Convert CSV file to Vowpal Wabbit format.')
    parser.add_argument("--input_file",  help="path to csv input file")
    parser.add_argument("--output_file", help="path to output file")
    parser.add_argument("--lang", help="path to output file")
    '''parser.add_argument("--fields", nargs='+',  help="list of fields to sue")'''

    args = parser.parse_args()

    print("/ARGS : ", args)
    '''
    with open(args.input_file, 'r') as input_file:
        headers = get_headers(input_file, '\t')
        csv_reader = get_dict_reader(input_file, headers, '\t')
        dict_rows = get_csv_rows_materialized(csv_reader)
        pool = multiprocessing.Pool(10)
        for row in dict_rows:
            pool.apply_async(
                write_dict_row_to_vw_file_atomic,
                args=(row, args.output_file),
                kwds=dict(lang=args.lang, fields=args.fields, category_name='category_id'))
    pool.close()
    pool.join()
    '''
    i = 0
    vw_reader = VwReader(open(args.input_file, 'r'), ' ')
    bag_of_words_dict = {}
    for bow in vw_reader:
        bag_of_words_dict.update(bow)
        i+=1
        if not i%1e5:
            print('{0}\r'.format(i))
            sys.stdout.flush()
    from text_representations.bag_of_words import BagOfWords

    bag_of_words = BagOfWords(language='en').from_counter(bag_of_words_dict)
    import pickle

    # Here's an example dict
    # Use dumps to convert the object to a serialized string
    serial_grades = pickle.dump(bag_of_words, open(args.output_file, 'w'))


if __name__ == "__main__":
    import codecs, os
    from polyglot.detect import Detector
    path_to_texts = os.environ["HOME"]+"/BSUIR/sca/analytics/data/txt1"
    path_to_vw_all = os.environ["HOME"]+"/BSUIR/sca/analytics/topic_modeling/0-20000.vw_all"
    from langdetect import detect, DetectorFactory
    DetectorFactory.seed = 42
    already_processed = set()

    '''with open("0-2000_unique.vw", "a") as vowpal_wabbit_file_unique:
        with open("0-2000.vw", "r") as vowpal_wabbit_file:
            for line in vowpal_wabbit_file:
                line_id = line.split(' ')[0]
                if line_id not in already_processed:
                    vowpal_wabbit_file_unique.write(line)
                already_processed.update({line.split(' ')[0]})'''

    '''with open("0-20000.vw_1", "a") as vowpal_wabbit_file:
        for text_file in os.listdir(path_to_texts):
            try:
                with codecs.open(os.path.join(path_to_texts, text_file), "r", encoding='utf-8', errors='ignore') as current_text_file:
                    current_text = current_text_file.read().replace('\n', '')

                    #if current_text.split(' ')[0] in already_processed:
                    #     continue
                    cur_text_lang = detect(current_text)
                    print(text_file + " LANG : ", cur_text_lang)#cur_text_lang)
                    if detect(current_text) in ('en', 'ru'):
                        print("post to corpus line")
                        vowpal_wabbit_file.write((post_to_corpus_line(
                            {
                                "id": text_file.split('.')[0],
                                "content": Text(current_text, cur_text_lang)
                            },
                            fields=("content",)
                        )))
                os.remove(os.path.join(path_to_texts, text_file))

            except:
                pass'''
    with open("0-20000.vw_rus", "a") as vowpal_wabbit_rus:
        with open(path_to_vw_all, "r") as vowpal_wabbit_all:
            for line in vowpal_wabbit_all:
                if detect(line) not in ('en'):
                    vowpal_wabbit_rus.write(line)


    #delete_stop_words_from_wv_file("0-2000_unique.vw")
