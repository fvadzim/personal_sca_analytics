from __future__ import print_function
import sys
import os
import json

cur_file_path = os.path.abspath(__file__)
sys.path.append(cur_file_path)
sys.path.append(os.path.dirname(cur_file_path))
sys.path.append(os.path.dirname(os.path.dirname(cur_file_path)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(cur_file_path))))
from analytics.filter.filter import get_filter
from analytics.text_representations.bag_of_words import BagOfWords

from analytics.convert.text_process import *
from analytics.convert.cur_csv_to_vw import construct_bow

from flask import Flask
from flask import request

app = Flask(__name__)


@app.route('/get_bag_of_words', methods = ['Get', 'POST'])
def get_bag_of_words():
    '''
    *Parameters *:
    ```python
    publication: {
        publication_id: string,
        language: string,
        text: string
    }
    ```
    *Returns *:
    ```python
    result: {
        publication_id: string,
        bag_of_words: string  # vowpal wabbit string
    }
    '''
    if not request.get_json():
        abort(400)
    publication = request.get_json()
    return json.dumps(
        {
            "publication_id": publication["publication_id"],
            "bag_of_words": construct_bow(BagOfWords(publication["text"], publication["language"]).get_counter()),
        })

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001, debug=True)
