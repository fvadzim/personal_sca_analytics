from __future__ import print_function
import sys
from scipy.spatial.distance import cosine
import os
cur_file_path = os.path.abspath(__file__)
sys.path.append(cur_file_path)
sys.path.append(os.path.dirname(cur_file_path))
sys.path.append(os.path.dirname(os.path.dirname(cur_file_path)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(cur_file_path))))
from analytics.filter.filter import get_filter
from analytics.text_representations.bag_of_words import BagOfWords

try:
    import queue
except ImportError:
    import Queue as queue
import Pyro4.core
from analytics.convert.text_process import *
from analytics.convert.cur_csv_to_vw import construct_bow


Pyro4.config.SERIALIZER = 'pickle'
Pyro4.config.SERIALIZERS_ACCEPTED.add('pickle')



@Pyro4.behavior(instance_mode="single")
class FilterServer(object):
    def __init__(self):
        self.filter_russian = get_filter('ru')
        self.filter_english = get_filter('en')

    @Pyro4.expose
    def get_filtered_text(self, text, language):
        if language.lower() not in ('en', "english", 'ru', "russian"):
            raise KeyError("Lang should be ru or en")

        if language in ('ru', 'russian'):
            return self.filter_russian.get_tokens(text)
        try:
            print(
                    Counter(self.filter_english.get_tokens(text)), filtering=False).items()
        except:
            pass
        return (' '.join(self.filter_english.get_tokens(text)))

    @Pyro4.expose
    def get_bag_of_words(self, text, language):
        '''
        :param text: str
        :return: dict key: value  token: number of accurancies
        '''
        return BagOfWords(text, language)

    @Pyro4.expose
    def get_bag_of_words_serialized(self, text, language):
        '''
        :param text: str
        :return: string of kind "token0: 2 token1: 3 token3"
        '''
        return construct_bow(BagOfWords(text, language).get_counter())


def main():
    # HOST:PORT
    address = str(sys.argv[1]).split(':')
    host = address[0]
    port = int(address[1])

    daemon = Pyro4.core.Daemon(host, port)
    master = FilterServer()
    uri = daemon.register(master, "master")

    print("Master is running: " + str(uri))
    daemon.requestLoop()


if __name__=="__main__":
    main()
