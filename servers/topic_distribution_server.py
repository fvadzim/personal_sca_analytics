from __future__ import print_function
import sys
import numpy as np
import os, sys
import numpy as np
import copy
import pickle
import pandas as pd
from scipy.spatial.distance import cosine
from os import path

cur_file_path = path.abspath(__file__)
sys.path.append(cur_file_path)

for i in range(3):
    sys.path.append(path.dirname(sys.path[-1]))

print(sys.path)

from analytics.filter.filter import get_filter
from analytics.topic_modeling.model import TopicDestribution




try:
    import queue
except ImportError:
    import Queue as queue
import Pyro4.core
from analytics.convert.text_process import *




Pyro4.config.SERIALIZER = 'pickle'
Pyro4.config.SERIALIZERS_ACCEPTED.add('pickle')


@Pyro4.behavior(instance_mode="single")
class TopicDistributionServer(object):
    def __init__(self, topic_distribution):
        self.model = None
        self.dimension = 20
        self._topic_distribution = topic_distribution

    @Pyro4.expose
    def get_topic_distribution(self, text):
        return self._topic_distribution.get_distribution(text).tolist()




def main():
    # HOST:PORT
    address = str(sys.argv[1]).split(':')
    host = address[0]
    port = int(address[1])

    daemon = Pyro4.core.Daemon(host, port)

    prev_to_prev_to_current_file = path.dirname(os.path.dirname(__file__))
    with open(path.join(prev_to_prev_to_current_file, "data/wiki_model_no_regs"), 'rb') as kyky_file:
        my_model = pickle.load(kyky_file)

    wiki_topic_destribution = TopicDestribution(my_model, 'en')
    master = TopicDistributionServer(wiki_topic_destribution)
    uri = daemon.register(master, "master")

    print("Master is running: " + str(uri))
    daemon.requestLoop()


if __name__=="__main__":
    main()