import os, sys
import numpy as np
import copy
import pickle
import pandas as pd
from scipy.spatial.distance import cosine
cur_file_path = os.path.abspath(__file__)
sys.path.append(cur_file_path)
sys.path.append(os.path.dirname(cur_file_path))
sys.path.append(os.path.dirname(os.path.dirname(cur_file_path)))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(cur_file_path))))